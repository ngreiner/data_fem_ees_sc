The present folder contains scanned pictures of cross-sections
of the monkey spinal cord at segmental levels C2 to T6.
The pictures were taken from  [Atlas of the Spinal Cord.
Sengul, Watson, Tanaka, Paxinos. 2013. Elsevier].